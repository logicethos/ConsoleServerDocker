using System.Text;
using System.Threading.Channels;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using ILogger = Serilog.ILogger;


namespace ConsoleServerDocker;

public sealed class clsDockerAttachToContainer : IDisposable
{
    private ILogger _logger;
    byte[] buffer = new byte[1024];
    
    public string ContainerID { get; private set; }
    public bool CloseOnExit { get; private set; }
    private AnsiTelnetConsole ANSIConsole;

    AutoResetEvent _waitForString = new AutoResetEvent(false);
    private bool _captureInput;
    public List<clsMatchItem> MatchItems { get; set; }

    private CancellationTokenSource CancelTokenSource = new CancellationTokenSource();
    public MultiplexedStream _stream;
    private DockerClient _client;

    public long DataCountRX { get; private set; }
    public long DataCountTX { get; private set; }
    public DateTime DataLastRX { get; private set; }
    Task resizeTask = null;
    private readonly object resizeTaskLock = new object();
    private Task listenTask;

    public CancellationToken _token
    {
        get
        {
            return CancelTokenSource.Token;
        }
    }

    public clsDockerAttachToContainer(bool closeOnExit, AnsiTelnetConsole ansiConsole = null)
    {
        CloseOnExit = closeOnExit;
        _logger = Log.Logger.ForContext(typeof(clsDockerRunOnHost));
        _client = clsDockerManager._client;
        ANSIConsole = ansiConsole;
    }

        /// <summary>
    /// Attach streams to container.  Start container if necessary.
    /// </summary>
    /// <param name="containerID"></param>
    /// <param name="commands"></param>
    /// <returns></returns>
    public bool AttachToContainer(string containerID, string[] commands = null)
    {
        ContainerID = containerID;
        try
        {
            var IsRunning = _client.Containers.InspectContainerAsync(containerID).Result;
            if (!IsRunning.State.Running)
            {
                var result2 = _client.Containers.StartContainerAsync(ContainerID, new ContainerStartParameters()).Result;
                _logger.Information("StartContainerAsync: {result2}",result2);
            }

            if (commands?.Length > 0)
            {
                var execParams = new ContainerExecCreateParameters()
                {
                    AttachStdin = true,
                    AttachStderr = true,
                    AttachStdout = true,
                    Tty = true,
                    Cmd = commands,
                };

                if (ANSIConsole != null) execParams.Env = new List<string>() { $"COLUMNS={ANSIConsole.Profile.Width}", $"LINES={ANSIConsole.Profile.Height}" };

                var exec = _client.Exec.ExecCreateContainerAsync(containerID, execParams).Result;
                _stream = _client.Exec.StartAndAttachContainerExecAsync(exec.ID, true).Result;
           //  if (ANSIConsole != null) WriteCommand($"printf '\\e[8;{ANSIConsole.Profile.Height};{ANSIConsole.Profile.Width}t'").Wait();

            }
            else
            {
                var config = new ContainerAttachParameters
                {
                    Stream = true,
                    Stderr = true,
                    Stdin = true,
                    Stdout = true
                };

                _stream = _client.Containers.AttachContainerAsync(ContainerID, true, config, CancelTokenSource.Token).Result;
             //   if (ANSIConsole != null) WriteCommand($"resize -s {ANSIConsole.Profile.Height} {ANSIConsole.Profile.Width} > /dev/null").Wait();
            }

            if (ANSIConsole!=null) ANSIConsole.TelnetSession.ScreenResize += TelnetSessionOnScreenResize;

            return true;
        }
        catch (Exception e)
        {
            _logger.Error(e,nameof(AttachToContainer));
        }

        return false;
    }

    private void TelnetSessionOnScreenResize(object? sender, (int Width, int Height) e)
    {
        lock (resizeTaskLock)
        {
            if (resizeTask?.IsCompleted ?? true)
            {
                resizeTask = Task.Run(() =>
                {
                    try
                    {
                        Task.Delay(500).Wait();

                        _client.Containers.ResizeContainerTtyAsync(ContainerID, new ContainerResizeParameters
                        {
                            Height = ANSIConsole.Profile.Height,
                            Width = ANSIConsole.Profile.Width
                        });
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Unable to resize {message}", ex.Message);
                    }
                });
            }
        }
    }


    public Task AttachConsoleInOut(bool waitForOutput, int millisecondTimout = 10000,params clsMatchItem[] matchItem)
    {
        if (ANSIConsole == null) throw new Exception("Console is Null");

        listenTask = ListenTask(waitForOutput,millisecondTimout,matchItem);

        try
        {
            ANSIConsole.Input.KeyPressed += HandleKeyPressEvent;
            _captureInput = true;
        }
        catch (DockerApiException ex)
        {
            ANSIConsole?.WriteLine(ex.Message, "red");
            Log.Error("{name} Attach {message} ", nameof(AttachConsoleInOut), ex.Message);
        }
        catch (Exception ex)
        {
            ANSIConsole?.WriteException(ex);
        }

        return listenTask;
    }

    /// <summary>
    /// Starts Task to reads container output, and sends to Console until the stream is closed.
    /// </summary>
    /// <param name="ansiConsole"></param>
    public Task AttachConsoleOutput(bool waitForOutput, int millisecondTimout = 10000, params clsMatchItem[] matchItem)
    {
        if (ANSIConsole == null) throw new Exception("Console is Null");

        listenTask = ListenTask(waitForOutput, millisecondTimout,matchItem);

        return listenTask;
    }

    Task ListenTask(bool waitForOutput,int millisecondTimout,clsMatchItem[] matchItem)
    {
        if (matchItem?.Length > 0)
        {
            MatchItems = matchItem.ToList();
        }

        var outputStarted = new ManualResetEventSlim();
        var listenTask = Task.Run(async () =>
        {
            MultiplexedStream.ReadResult output;
            var buffer = new byte[1024];

            do
            {
                output = await _stream.ReadOutputAsync(buffer, 0, buffer.Length, CancelTokenSource.Token);

                if (output.Count > 0)
                {
                    DataCountRX += output.Count;
                    DataLastRX = DateTime.UtcNow;
                    ANSIConsole.TelnetSession.SendFixCR(buffer, 0, output.Count);

                    if (MatchItems?.Count > 0)
                    {
                        var text = Encoding.UTF8.GetString(buffer, 0, output.Count);
                        foreach (var item in MatchItems)
                        {
                            if (item.Match(text))
                            {
                                _waitForString.Set();
                                break;
                            }
                        }
                    }
                    outputStarted.Set();
                }
                outputStarted.Set();
            } while (!output.EOF);
        },CancelTokenSource.Token);

        if (waitForOutput) outputStarted.Wait(millisecondTimout);
        return listenTask;
    }


    /// <summary>
    /// Wait for text match (true) or inactivity (false)
    /// </summary>
    /// <param name="maxInactivity"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    public bool WaitForMatch(TimeSpan maxInactivity, params string[] text)
    {
        if (text?.Length > 0)
        {
            return WaitForMatch(maxInactivity,text);
        }
        return WaitForMatch(maxInactivity);
    }

    /// <summary>
    /// Wait for text match (true) or inactivity (false)
    /// </summary>
    /// <param name="maxInactivity"></param>
    /// <param name="matchItem"></param>
    /// <returns></returns>
    public bool WaitForMatch(TimeSpan maxInactivity, params clsMatchItem[] matchItem)
    {
        if (matchItem?.Length > 0)
        {
            MatchItems = matchItem.ToList();
        }
        return WaitForMatch(maxInactivity);
    }

    /// <summary>
    /// Wait for text match (true) or inactivity (false)
    /// </summary>
    /// <param name="maxInactivity"></param>
    /// <returns></returns>
    public bool WaitForMatch(TimeSpan maxInactivity)
    {
        var nextTimespan = maxInactivity;

        while (!_waitForString.WaitOne(nextTimespan))
        {
            nextTimespan = maxInactivity - (DateTime.UtcNow - DataLastRX);
            if (nextTimespan.Milliseconds < 0) return false;
        }

        return true;
    }


    public void WaitForExitSilence(int silenceSeconds, int maxSeconds = Int32.MaxValue)
    {
        var dataRx = DataCountRX;
        int silenceCount = 0;
        int maxLoopCount = 0;
        do
        {
            if (!listenTask.Wait(1000,_token)) return;
            if (DataCountRX == dataRx) silenceCount++;
            else dataRx = DataCountRX;
            maxLoopCount++;

        } while (silenceCount < silenceSeconds && maxLoopCount < maxSeconds && !_token.IsCancellationRequested);
    }

    void HandleKeyPressEvent(object sender, ConsoleKeyInfoExt info)
    {

        //http://www.embedded-os.de/en/proto-vt100.shtml

        if (info.TelnetBytes != null)
        {
            Write(info.TelnetBytes);
        }
        else
        {
            Write(info.KeyChar);
        }
        info.Cancel = true;
    }  



    public Task ReadToLog()
    {
        return Task.Run(async () =>
        {
            var byteArray = new byte[1024];
            MultiplexedStream.ReadResult readResult;
            do
            {
                readResult = await Read(byteArray);
                if (readResult.Count > 0)
                {
                    var text = Encoding.UTF8.GetString(byteArray, 0, readResult.Count);
                    _logger.Information(text);
                }
            } while (!readResult.EOF);
            _logger.Information("Docker detached");
        }, CancelTokenSource.Token);
    }
    
    
    public async Task<MultiplexedStream.ReadResult> Read(byte[] buffer)
    {
        return await _stream.ReadOutputAsync(buffer,0,buffer.Length,CancelTokenSource.Token);
    }
    
    public IEnumerable<string> ReadString()
    {
        var output = _stream.ReadOutputAsync(buffer,0,buffer.Length,CancelTokenSource.Token).Result;
        if (output.EOF) yield break;
        DataCountRX += output.Count;
        yield return Encoding.UTF8.GetString(buffer, 0, output.Count);
    }

    public async Task<bool> Close()
    {
        if (String.IsNullOrEmpty(ContainerID))
        {
            try
            {
                _logger.Debug("{class}: Closing Container",nameof(clsDockerRunOnHost));
                return await _client?.Containers.StopContainerAsync(ContainerID, new ContainerStopParameters(), CancelTokenSource.Token);
            }
            catch (Exception e)
            {
                _logger.Error(e,"",nameof(clsDockerRunOnHost));
            }
        }
        return false;
    }
    
    public async Task Write(byte[] telnetBytes)
    {
        await _stream?.WriteAsync(telnetBytes,0, telnetBytes.Length, CancelTokenSource.Token);
    }

    public async Task Write(char infoKeyChar)
    {
        var bytes = BitConverter.GetBytes(infoKeyChar);

        await _stream?.WriteAsync(bytes,0, bytes[1]==0 ? 1 : 2, CancelTokenSource.Token);
        DataCountTX += bytes.Length;
    }
    
    public async Task Write(string text)
    {
        var bytes = Encoding.ASCII.GetBytes($"{text}");
        await _stream?.WriteAsync(bytes,0, bytes.Length, CancelTokenSource.Token);
        DataCountTX += bytes.Length;
    }
    
    public async Task WriteCommand(string command)
    {
        var bytes = Encoding.ASCII.GetBytes($"{command}\n");
        await _stream?.WriteAsync(bytes,0, bytes.Length, CancelTokenSource.Token);
        DataCountTX += bytes.Length;
        _logger.Verbose(command);
    }
    
    public void Dispose()
    {
        CancelTokenSource.Cancel(true);

        if (ANSIConsole != null)
        {
            ANSIConsole.TelnetSession.ScreenResize -= TelnetSessionOnScreenResize;

            if (_captureInput)
            {
                ANSIConsole.Input.ClearInput();
                ANSIConsole.Input.KeyPressed -= HandleKeyPressEvent;
            }
        }

        if (CloseOnExit) Close().Wait();
        _stream?.Dispose();
        CancelTokenSource.Dispose();
    }


}