namespace ConsoleServerDocker;

public enum MatchItemState
{
    NoMatch,
    Matched,
    Matched_Positive,
    Matched_Negative,
}

public class clsMatchItem
{
    public String[] TextMatch { get; set; }
    public uint Count { get; set; }
    public Action<string> MatchAction;
    public MatchItemState ItemState = MatchItemState.NoMatch;
    public MatchItemState SetMatchedState = MatchItemState.Matched;


    public clsMatchItem(params String[] text)
    {
        TextMatch = text;
    }

    public clsMatchItem(Action<string> matchAction = null,params String[] text)
    {
        TextMatch = text;
        MatchAction = matchAction;
    }

    public clsMatchItem(MatchItemState matchedItemState,params String[] text)
    {
        TextMatch = text;
        SetMatchedState = matchedItemState;
    }

    public bool Match(String text)
    {
        if (TextMatch.Any(x=>text.Contains(x)))
        {
            Count++;
            MatchAction?.Invoke(text);
            if (ItemState == MatchItemState.NoMatch) ItemState = SetMatchedState;
            return true;
        }
        return false;
    }

}