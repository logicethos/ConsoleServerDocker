using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Serilog;
using ILogger = Serilog.ILogger;

namespace ConsoleServerDocker;

public class clsDockerImageNsenter : IDockerImage
{
    private AnsiTelnetConsole ANSIConsole;
    
    ImagesCreateParameters AlpineImage = new ImagesCreateParameters
    {
        FromImage = "alpine:3.16",
        Repo = "alpine",
        Tag = "3.16",
    };

    public clsDockerImageNsenter(AnsiTelnetConsole console)
    {
        ANSIConsole = console;
    }

    public CreateContainerResponse Create(string[] entrypoint, string? tag = null)
    {

        IProgress<JSONMessage> progress = (ANSIConsole != null) ? new clsProgressOutput(ANSIConsole) : new Progress();

        //Pull Image
        clsDockerManager.Instance.CreateImageAsync(AlpineImage, new AuthConfig(), progress).Wait();
            
        //Set container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = null,
            Env = null,
            Cmd = null,
            ArgsEscaped = false,
            Image = AlpineImage.FromImage,
            Entrypoint = entrypoint,
            Labels = null,
            AttachStderr = true,
            AttachStdin = true,
            AttachStdout = true,
            OpenStdin = true,
            StdinOnce = true,
            Tty = true,
            HostConfig = new HostConfig
            {
                Binds = new List<string>()
                {
                    "/var/run/docker.sock:/var/run/docker.sock",
                },
                Privileged = true,
                AutoRemove = true,
                PidMode = "host",
                CgroupnsMode = "host",
                ReadonlyRootfs = false,
            }
        };
        
        if (!String.IsNullOrEmpty(runParams.Name)) clsDockerManager.Instance.DeleteContainerIfExist(runParams.Name);
        //Create container
        var result =  clsDockerManager.Instance.CreateContainerAsync(runParams).Result;
        
        Log.Logger.Information("{clsDockerImageNsenter}: {result}",nameof(clsDockerImageNsenter),result);
        foreach (var warn in result.Warnings)
        {
            Log.Logger.Warning("{image} {warn}",AlpineImage.Repo,warn);
        }
        
        return result;
    }
    
}