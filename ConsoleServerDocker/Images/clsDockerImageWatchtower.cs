using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Serilog;
using ILogger = Serilog.ILogger;

namespace ConsoleServerDocker;

public class clsDockerImageWatchtower : IDockerImage
{
    private AnsiTelnetConsole ANSIConsole;
    
    ImagesCreateParameters WatchtowerImage = new ImagesCreateParameters
    {
        FromImage = "containrrr/watchtower",
        Tag = "latest",
    };

    public clsDockerImageWatchtower(AnsiTelnetConsole console)
    {
        ANSIConsole = console;
    }
    

    public CreateContainerResponse Create(string dockerID, AnsiTelnetConsole console = null)
    {

        IProgress<JSONMessage> progress = (console != null) ? new clsProgressOutput(console) : new Progress();

        //Pull Image
        clsDockerManager.Instance.CreateImageAsync(WatchtowerImage, new AuthConfig(), progress);
            
        //Set Watchtower container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = null,
            Env = null,
            Cmd = new string[] { "--cleanup","--run-once", dockerID },
            ArgsEscaped = false,
            Image = WatchtowerImage.FromImage,
            Entrypoint = null,
            Labels = null,
            AttachStdout = (console != null),
            Tty = (console != null),
            HostConfig = new HostConfig()
            {
                Binds = new List<string>()
                {
                    "/var/run/docker.sock:/var/run/docker.sock",
                },
                AutoRemove = true
            }
        };

        if (!String.IsNullOrEmpty(runParams.Name)) clsDockerManager.Instance.DeleteContainerIfExist(runParams.Name).Wait();
        
        //Create Watchtower container
        var result = clsDockerManager.Instance.CreateContainerAsync(runParams).Result;
        
        Log.Information("CreateContainerAsync: {result}",result);
        foreach (var warn in result.Warnings)
        {
            Log.Warning("Warning: {warn}",warn);
        }

        return result;

    }

}