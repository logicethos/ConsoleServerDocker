using System;
using System.Net.Http.Json;
using System.Text;
using ConsoleServerDocker;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using ILogger = Serilog.ILogger;
using TaskStatus = System.Threading.Tasks.TaskStatus;

namespace ConsoleServerDocker;


public sealed class clsDockerManager : IDisposable
{
    const string cgroupPath = "/proc/self/cgroup";
    static public clsDockerManager Instance { get; private set; }
    
    public String DockerID { get; private set; }
    public String DockerName { get; init; }
    public String DockerImagePath { get; init; }
    public static DockerClient _client { get; private set; }
    private CancellationTokenSource CancelTokenSource;
    
    public clsDockerManager(string dockerName, string dockerImagePath, CancellationTokenSource cancelTokenSource, string dockerSocket="unix:///var/run/docker.sock")
    {
        Instance = this;
        DockerName = dockerName;
        CancelTokenSource = cancelTokenSource;
        _client = new DockerClientConfiguration(new Uri(dockerSocket)).CreateClient();
        if (_client == null) throw new Exception("Docker failed to connect {dockerSocket}");
      //  DockerID = GetLocalContainerID(); broken
      DockerID = DockerName;  //TODO
      DockerImagePath = dockerImagePath;
    }

    /// <summary>
    /// Lists Containers Locally.
    /// </summary>
    public async Task<IList<ContainerListResponse>> ListContainers(long limit)
    {
        return  await _client.Containers.ListContainersAsync(new ContainersListParameters() { Limit = limit, });
    }

    public async Task ListTags()
    {
        var image = await _client.Images.GetImageHistoryAsync("containrrr/watchtower");
        foreach (var reply in image)
        {
            Console.WriteLine($"ID: {reply.ID}  Comment: {reply.Comment}  Created: {reply.CreatedBy}");
        }
    }

    public clsDockerRunOnHost CreateRunOnHostInstance(AnsiTelnetConsole console = null, CancellationToken? cancelToken = null)
    {
        return new clsDockerRunOnHost(console, _client, cancelToken);
    }


    /// <summary>
    /// Get Local Container ID
    /// </summary>
    /// <returns>ID or Null</returns>
    private string GetLocalContainerID()
    {
        if (File.Exists(cgroupPath))
        {
            var lines = File.ReadLines(cgroupPath);
            foreach (var line in lines)
            {
                var pt1 = line.LastIndexOf("/");
                var len = line.Length - pt1;
                if (pt1 == 64) return line.Substring(pt1, 64);
            }
        }

        return null;
    }

    public async Task<bool> PullLatestVersion(AnsiTelnetConsole console = null)
    {
        IProgress<JSONMessage> progress = (console != null) ? new clsProgressOutput(console) : new Progress();
        ImagesCreateParameters ACSharpImage = new ImagesCreateParameters
        {
            FromImage = DockerImagePath,
            Tag = "latest"
        };
        //Pull Image
        await CreateImageAsync(ACSharpImage, new AuthConfig(), progress);
        return ((clsProgressOutput)progress).DownloadStarted;
    }


    /// <summary>
    /// Restart docker container pulling latest version.
    /// </summary>
    /// <param name="dockerID">Optional Docker ID/Name.  Default is this container.</param>
    public async Task<bool> UpgradeContainer(AnsiTelnetConsole console, string? dockerID = null)
    {
        try
        {
            var pulled = PullLatestVersion(console).Result;

            var result = new clsDockerImageWatchtower(console).Create(dockerID ?? DockerID ?? DockerName);

            //Run Watchtower
            var result2 = await _client.Containers.StartContainerAsync(result.ID, new ContainerStartParameters());
            Log.Information("StartContainerAsync: {result2} {id}", result2, dockerID ?? DockerID ?? DockerName);

            try
            {

                using (var container = new clsDockerAttachToContainer(true,console))
                {
                    if (container.AttachToContainer(result.ID))
                    {
                        var consoleTask = container.AttachConsoleOutput(true,1000);
                        container.WaitForExitSilence(3,10);
                    }
                }
            }
            catch (DockerApiException ex)
            {
                console.WriteLine(ex.Message, "red");
                Log.Error("{name} RunInit {message} ", nameof(UpgradeContainer), ex.Message);
            }
            catch (Exception ex)
            {
                console.WriteException(ex);
            }

            console.MarkupLine("[yellow]restarting......this session will disconnect.[/]");
            CancelTokenSource.Cancel();
        }
        catch (Exception e)
        {
            Log.Error(e,nameof(UpgradeContainer));
        }

        return false;
    }

    public async Task<VolumeResponse[]?> GetVolumes(string substring)
    {
        var dockerVolumes = await _client.Volumes.ListAsync();
        return dockerVolumes?.Volumes.Where(x => x.Name.ToLower().Contains(substring)).ToArray() ?? null;
    }

    
    public async IAsyncEnumerable<string> ReadLogs(string containerId, CancellationToken token = default)
    {
        var parameters = new ContainerLogsParameters
        {
            ShowStdout = true,
            ShowStderr = true,
            Since = null,
            Timestamps = true,
            Follow = true,
            Tail = "300",
        };
        
        var logStream = await _client?.Containers?.GetContainerLogsAsync(containerId, parameters, token);
        if (logStream != null)
        {
            using (var reader = new StreamReader(logStream, new UTF8Encoding(false)))
            {
                while (!reader.EndOfStream)
                {
                    yield return await reader.ReadLineAsync();
                }
            }
        }
    }

    public async IAsyncEnumerable<string> GetLogOutput(CreateContainerResponse container, int maxSeconds = -1, CancellationToken token = default)
    {
        var timeTo = DateTime.UtcNow.AddSeconds(maxSeconds);
        
        var parameters = new ContainerLogsParameters
        {
            ShowStdout = true,
            ShowStderr = true
        };

        var buffer = new char[1024];
        
        var logStream = await _client?.Containers?.GetContainerLogsAsync(container.ID, parameters, token);
        if (logStream != null)
        {
            using (var reader = new StreamReader(logStream))
            {
                Task<int> readTask;
                do
                {
                    readTask = reader.ReadAsync(buffer, 0, buffer.Length);
                    if (Task.WaitAny(new[]{readTask}, maxSeconds > 0 ? (int)(timeTo-DateTime.UtcNow).TotalMilliseconds:-1) == -1) yield break;
                    if (readTask.Result==0) yield break;
                    yield return new string(buffer, 0, readTask.Result);
                } while (true);
            }
        }
    }
    
    public void Dispose()
    {
        _client.Dispose();
    }

    /// <summary>
    /// RunContainer, optional wait. 
    /// </summary>
    /// <param name="container"></param>
    /// <param name="waitMilliseconds"> if >0 then waits for completion or fails. </param>
    /// <param name="token"></param>
    /// <returns></returns>
    public async Task<bool> RunContainer(CreateContainerResponse container, int waitMilliseconds = 0, CancellationToken token = default)
    {
        foreach (var containerWarning in container.Warnings)
        {
            Log.Warning(containerWarning);
        }
        
        if (await _client.Containers.StartContainerAsync(container.ID, new ContainerStartParameters(),token))
        {
            Log.Information("RunContainer: {container}", container.ID);

            if (waitMilliseconds > 0)
            {
                var result2 = _client.Containers.WaitContainerAsync(container.ID,token);
                return (Task.WaitAny(new[] {result2}, waitMilliseconds) != -1);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task CreateImageAsync(ImagesCreateParameters image, AuthConfig authConfig, IProgress<JSONMessage> progress)
    {
         await _client.Images.CreateImageAsync(image, new AuthConfig(), progress);
    }

    public async Task<CreateContainerResponse> CreateContainerAsync(CreateContainerParameters runParams)
    {
        return await _client.Containers.CreateContainerAsync(runParams);
    }

    public async Task<VolumeResponse?>GetVolumeResponse(string volume)
    {
        try
        {
            return await _client.Volumes.InspectAsync(volume);
        }
        catch (Exception e)
        {
            return null;
        }
    }
    
    public async Task<ContainerListResponse?> GetContainer(string name)
    {
       
        var result = await _client.Containers.ListContainersAsync(new ContainersListParameters(){All = true});
        
        foreach (var container in result)
        {
            foreach (var containerName in container.Names)
            {
                if (containerName.Contains(name)) return container;
            }
        }
        return null;
    }

    public DockerClient GetClient()
    {
        return _client;
    }

    public Task Delete(ContainerListResponse container)
    {
        var parameters = new ContainerRemoveParameters()
        {
            Force = true
        };
        return _client.Containers.RemoveContainerAsync(container.ID,parameters);
    }

    public async Task<bool> DeleteVolume(string volumeName)
    {
        try
        {
            await _client.Volumes.RemoveAsync(volumeName, true);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public async Task DeleteContainerIfExist(string name)
    {
        if (string.IsNullOrEmpty(name)) return;
        try
        {
            var result = await _client.Containers.ListContainersAsync(new ContainersListParameters() { All = true});
            if (result != null)
            {
                foreach (var container in result)
                {
                    if (container.Names.Any(x => x.Contains(name)))
                    {
                        await clsDockerManager.Instance.Delete(container);
                    }
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public void StopContainer(string containerId)
    {
        _client.Containers.StopContainerAsync(containerId, new ContainerStopParameters()).Wait();
    }

    public void AttatchToLogOutput(string containerID, AnsiTelnetConsole ansiConsole)
    {
        try
        {
            var parameters = new ContainerLogsParameters
            {
                ShowStdout = true,
                ShowStderr = true,
                Since = null,
                Timestamps = true,
                Follow = true,
                Tail = "300",
            };
            var container = GetContainer(containerID).Result;
            if (container != null)
            {
                AnsiConsole.MarkupLine("[yellow]Press ESCAPE to exit[/]");
                byte[] buffer = new byte [1024];
                using (var logStream = GetClient()?.Containers?.GetContainerLogsAsync(container.ID, parameters).Result)
                {
                    using (var keypress = new clsKeyPressed(ansiConsole, new Action<ConsoleKeyInfoExt>(c =>
                           {
                               c.Cancel = true;
                               if (c.Key == ConsoleKey.Escape ||
                                   c.Shift ||
                                   c.Control)
                               {
                                   logStream.Close();
                               }
                           })))
                    {
                        try
                        {
                            if (logStream != null)
                            {
                                do
                                {
                                    var count = logStream.Read(buffer, 0, buffer.Length);
                                    if (count <= 0) break;
                                    ansiConsole.TelnetSession.SendFixCR(buffer, 0, count);
                                } while (true);
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
            }
            else
            {
                AnsiConsole.WriteLine("Not Found");
            }
        }
        catch (Exception ex)
        {
            AnsiConsole.WriteException(ex);
            throw;
        }
    }
}

public class Progress : IProgress<JSONMessage>
{
    public void Report(JSONMessage value)
    {
        Console.WriteLine(value.Status);
    }
}
