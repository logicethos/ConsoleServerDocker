using System.Text;
using System.Threading.Channels;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using ILogger = Serilog.ILogger;


namespace ConsoleServerDocker;

public sealed class clsDockerRunOnHost : IDisposable
{
    private ILogger _logger;
    private DockerClient _client { get; init; }
    
    public string ContainerID { get; private set; }
    
    
    private CancellationToken _token;
    private MultiplexedStream _stream;
    private AnsiTelnetConsole _console;
    

    public clsDockerRunOnHost(AnsiTelnetConsole console, DockerClient client,CancellationToken? token = null)
    {
        _logger = Log.ForContext(typeof(clsDockerRunOnHost));
        _client = client;
        _token = token ?? new CancellationToken();
        _console = console;
        if (_client == null) throw new Exception("Docker client not initialised");
    }
    //public async Task RunOnHost(string[] commands, CancellationToken token)
    public bool AttatchToHost(params string[] command)
    {
        //docker run --privileged --pid=host -it alpine:3.16 nsenter -t 1 -m -u -n -i iptable

        try
        {
            StartContainer(command);
            
            //_client.Containers.AttachContainerAsync(result.ID,true, new ContainerAttachParameters().)
                
            var config = new ContainerAttachParameters {
                Stream = true,
                Stderr = true,
                Stdin = true,
                Stdout = true
            };

            _stream = _client.Containers.AttachContainerAsync(ContainerID, true, config, _token).Result;
            
            return true;
        }
        catch (Exception e)
        {
            _logger.Error(e,nameof(AttatchToHost));
        }

        return false;
    }

    private void StartContainer(params string[] command)
    {
        var startCommand = new string[] { "/usr/bin/nsenter", "-t1", "-m", "-u", "-n", "-i" };
        startCommand = command.Length > 0 ? startCommand.Concat(command).ToArray() : startCommand.Concat(new []{"sh"}).ToArray();

            //Create container
            var result = new clsDockerImageNsenter(_console).Create(startCommand);
            ContainerID = result.ID;

            //Run
            var result2 = _client.Containers.StartContainerAsync(ContainerID, new ContainerStartParameters()).Result;
            _logger.Information("StartContainerAsync: {result2}", result2);
    }

    public Task StartReaderTask()
    {
        var readTask = Task.Run(async () =>
        {
            try
            {
                var byteArray = new byte[1024];
                MultiplexedStream.ReadResult readResult;
                do
                {
                    readResult = await _stream.ReadOutputAsync(byteArray,0,byteArray.Length,_token);
                    if (readResult.Count > 0)
                    {
                        var text = Encoding.UTF8.GetString(byteArray, 0, readResult.Count);
                        _console?.WriteLine(text);
                        _logger.Information(text);
                    }
                } while (!readResult.EOF);
            }
            catch (Exception e)
            {
                _console?.WriteLine($"StartReaderTask: {e.Message}");
                _logger.Information($"StartReaderTask: {e.Message}");
            }

            _logger.Information("Docker detached");
        }, _token);
        return readTask;
    }

    /*public async Task<MultiplexedStream.ReadResult> Read(byte[] buffer)
    {
        return await _stream.ReadOutputAsync(buffer,0,buffer.Length,_token);
    }*/

    public async Task<bool> Close()
    {
        if (String.IsNullOrEmpty(ContainerID))
        {
            try
            {
                _logger.Debug("{class}: Closing Container",nameof(clsDockerRunOnHost));
                return await _client?.Containers.StopContainerAsync(ContainerID, new ContainerStopParameters(), _token);
            }
            catch (Exception e)
            {
                _logger.Error(e,"",nameof(clsDockerRunOnHost));
            }
        }
        return false;
    }
    
    
    public void WriteCommand(string command)
    {
        if (!String.IsNullOrEmpty(ContainerID))
        {
            var bytes = Encoding.ASCII.GetBytes($"{command}\n");
            _stream.WriteAsync(bytes, 0, bytes.Length, _token).Wait();
            _logger.Verbose(command);
        }
        else
        {
            StartContainer(command);
        }
    }
    
    public void Dispose()
    {
        Close().Wait();
        _stream?.Dispose();
    }
}